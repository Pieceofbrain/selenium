package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import design.ProjectMethods;
import pages.LoginPage;

public class TC001_Login extends ProjectMethods {
	@BeforeTest
	public void setData() {
		dataSheetName="Login";
	}
	
	@Test(dataProvider="fetchData")
	public void Login(String uName, String pwd,String cName,String fName,String lname) {
		new LoginPage()
		.typeUserName(uName)
		.typePassword(pwd)
		.clickLogin()
		.clickcrmsfamenu().clickleads().clickcreatelead().typeCompanyname(cName).typeFirstname(fName).typeLastname(lname).clicksubmit();	
		

	}

}
