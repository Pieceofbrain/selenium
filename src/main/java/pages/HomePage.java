package pages;

import design.ProjectMethods;

public class HomePage extends ProjectMethods {

	public LoginPage clickLogOut() {
		driver.findElementByClassName("decorativeSubmit").click();
		return new LoginPage();
	}
	
	public Myhome clickcrmsfamenu() {
		driver.findElementByLinkText("CRM/SFA").click();
		return new Myhome();
		
	}
}
