package pages;

import design.ProjectMethods;

public class Createlead extends ProjectMethods{
	public Createlead typeCompanyname(String cName){
		driver.findElementById("createLeadForm_companyName").sendKeys(cName);
		return this;
		
	}
	public Createlead typeFirstname(String fName){
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
		return this;
	}
	public Createlead typeLastname(String lName){
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
		return this;
	}
	public Viewlead clicksubmit(){
		driver.findElementByName("submitButton").click();
		return new Viewlead();
	}
}
