package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import design.ProjectMethods;

public class LoginPage extends ProjectMethods {

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	// @CacheLookup
	@FindBy(how = How.ID, using = "username")
	WebElement eleUname;
	@FindBy(id = "password")
	WebElement elePwd;

	public LoginPage typeUserName(String uName) {
		// driver.findElementById("username").sendKeys(uName);
		eleUname.sendKeys(uName);
		return this;
	}

	public LoginPage typePassword(String pwd) {
		// driver.findElementById("password").sendKeys(pwd);
		elePwd.sendKeys(pwd);
		return this;

	}

	public HomePage clickLogin() {
		driver.findElementByClassName("decorativeSubmit").click();
		// HomePage hP = new HomePage();
		return new HomePage();
		}

		public LoginPage clickLoginnegative() {
			driver.findElementByClassName("decorativeSubmit").click();
			// HomePage hP = new HomePage();
			return this;
	}
}
