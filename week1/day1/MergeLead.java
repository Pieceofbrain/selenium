package week1.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch browser
		ChromeDriver driver =new ChromeDriver();
		//launch url
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		WebElement uname = driver.findElementById("username");
		
		//uname.sendKeys("DemoCSR");
		uname.sendKeys("DemoSalesManager");
		
		driver.findElementById("password").sendKeys("crmsfa");
		
		driver.findElementByXPath("//input[@type='submit']").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		Thread.sleep(4000);
		
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		
		
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		
		Set<String> allwindows = driver.getWindowHandles();
		
		List<String> listofwindows = new ArrayList <String> ();
		
		listofwindows.addAll(allwindows);
		
		driver.switchTo().window(listofwindows.get(1));
		
		System.out.println(driver.getTitle());
		
		//driver.switchTo().frame(0);
			
		
		driver.findElementByXPath("(//label[text()='Lead ID:']//following::input)[1]").sendKeys("10089");
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(2000);
		
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		
		Thread.sleep(2000);
		
		driver.switchTo().window(listofwindows.get(0));
		
		driver.findElementByXPath("(//input[@id='partyIdTo']/following::a)[1]").click();
		
Set<String> allwindows2 = driver.getWindowHandles();
		
		List<String> listofwindows2 = new ArrayList <String> ();
		
		listofwindows2.addAll(allwindows2);
		
		driver.switchTo().window(listofwindows2.get(1));
		
		System.out.println(driver.getTitle());
		
		driver.findElementByXPath("(//label[text()='Lead ID:']//following::input)[1]").sendKeys("10090");
		
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		
		Thread.sleep(2000);
		
		
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		
		Thread.sleep(2000);
		
		
		driver.switchTo().window(listofwindows2.get(0));
		
		driver.findElementByXPath("//a[@class='buttonDangerous']").click();
		
		
		Alert alt = driver.switchTo().alert();
		
		System.out.println(alt.getText());
		
		alt.accept();
		
		Thread.sleep(1000);
		
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		
		driver.findElementByXPath("(//label[text()='Lead ID:']//following::input)[1]").sendKeys("10089");
		
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(1000);
		
		String errmsg = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		
		System.out.println(errmsg);
		
		driver.close();
		
		



		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
