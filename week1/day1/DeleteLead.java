package week1.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DeleteLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch browser
		ChromeDriver driver =new ChromeDriver();
		//launch url
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		WebElement uname = driver.findElementById("username");
		
		uname.sendKeys("DemoCSR");
		
		driver.findElementById("password").sendKeys("crmsfa");
		
		driver.findElementByXPath("//input[@type='submit']").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		Thread.sleep(4000);
		
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByLinkText("Create Lead").click();
		
		//driver.findElementByXPath("//a[contains,text='Create Lead']").click();
		
		Thread.sleep(2000);
		
		String cname = "Accenture";

		
		WebElement Compname = driver.findElementById("createLeadForm_companyName");
				
				Compname.sendKeys(cname);

		driver.findElementById("createLeadForm_firstName").sendKeys("Vijay");
		
		driver.findElementById("createLeadForm_lastName").sendKeys("A");
		
		driver.findElementById("createLeadForm_dataSourceId").sendKeys("Cold Call");
		
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Vijay");
		
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("A");
		
		driver.findElementById("createLeadForm_personalTitle").sendKeys("test");
		
		
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("QualityAnalyst");
		
		driver.findElementById("createLeadForm_departmentName").sendKeys("IT");
		
		driver.findElementById("createLeadForm_industryEnumId").sendKeys("Aerospace");
		
		
		
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("50");
		
		Select eleownership = new Select (driver.findElementById("createLeadForm_ownershipEnumId"));
		
		eleownership.selectByIndex(2);
		
		
		driver.findElementById("createLeadForm_sicCode").sendKeys("13345");
		
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("&*@$");
		
		
		driver.findElementById("createLeadForm_description").sendKeys("description");
		
		driver.findElementById("createLeadForm_importantNote").sendKeys("text note");
		
		//contact information
		
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("001");
		
		
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("91");
		
		WebElement phonenum = driver.findElementById("createLeadForm_primaryPhoneNumber");

		String mobile = "9585533386";
		phonenum.sendKeys(mobile);
		
		
		
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("abc@test.com");
		
		
		
		
	//Primary Address
		
		driver.findElementById("createLeadForm_generalToName").sendKeys("VijayA");
		
		
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("100,1st Lane");
		
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("5th cross st");
		
		
		driver.findElementById("createLeadForm_generalCity").sendKeys("Miami");
		
		
		
		driver.findElementById("createLeadForm_generalStateProvinceGeoId").sendKeys("Florida");
		
		
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("33101");
		
		
		driver.findElementByXPath("//input[@name='submitButton']").click();
		
		Thread.sleep(2000);
		
		
		String vcompname =driver.findElementById("viewLead_companyName_sp").getText();
		
		System.out.println(vcompname);
		
		if (vcompname.contains(cname)) {
			
			System.out.println("matched");
			
		}	else {
				
				System.out.println("NotMatched");
				
				
			}
			
		//FindLead
		driver.findElementByLinkText("Find Leads").click();
		
		driver.findElementByXPath("//span[text()='Phone']").click();
		
		driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys(mobile);
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
		String leadID = driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").getText();
		
		System.out.println(leadID);
		
		Thread.sleep(2000);
		//driver.findElementByLinkText(leadID).click();
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").click();
		
		Thread.sleep(2000);
		
		driver.findElementByLinkText("Delete").click();
		
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//input[@name='id']").sendKeys(leadID);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
		
		String errormsg = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		
		System.out.println(errormsg);
		
	//driver.close();
		
		
		}
	
	
	

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
	
	//WebElement elesource = driver.findElementById("createLeadForm_marketingCampaignId");
	
	//Select marketdrop = new Select (elesource);
	
//List<WebElement> alloptions = marketdrop.getOptions();
	
	//int size = alloptions.size();
	
	//marketdrop.selectByVisibleText("Car and Driver");
	
	//marketdrop.selectByValue("CATRQ_CAMPAIGNS");
	
	//marketdrop.selectByIndex(size-1);
	
	/*for (WebElement eachoption : alloptions) {
		
		String displayoptions = eachoption.getText();
		
		System.out.println(displayoptions);
		
		
		
		
	}*/
	
	}


