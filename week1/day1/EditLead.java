package week1.day1;

import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class EditLead {

		@Test
	public void editLead() throws InterruptedException {
		// TODO Auto-generated method stub
		
		
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch browser
		ChromeDriver driver =new ChromeDriver();
		//launch url
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		WebElement uname = driver.findElementById("username");
		
		uname.sendKeys("DemoCSR");
		
		driver.findElementById("password").sendKeys("crmsfa");
		
		driver.findElementByXPath("//input[@type='submit']").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		Thread.sleep(4000);
		
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByLinkText("Create Lead").click();
		
		//driver.findElementByXPath("//a[contains,text='Create Lead']").click();
		
		Thread.sleep(2000);
		
		String cname = "BBC";

		
		WebElement Compname = driver.findElementById("createLeadForm_companyName");
				
				Compname.sendKeys(cname);

		driver.findElementById("createLeadForm_firstName").sendKeys("Jordi");
		
		driver.findElementById("createLeadForm_lastName").sendKeys("Alba");
		
		driver.findElementById("createLeadForm_dataSourceId").sendKeys("Cold Call");
		
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Jordi");
		
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Alba");
		
		driver.findElementById("createLeadForm_personalTitle").sendKeys("test");
		
		
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("JOURNALIST");
		
		driver.findElementById("createLeadForm_departmentName").sendKeys("IT");
		
		driver.findElementById("createLeadForm_industryEnumId").sendKeys("Press");
		
		
		
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("50");
		
		Select eleownership = new Select (driver.findElementById("createLeadForm_ownershipEnumId"));
		
		eleownership.selectByIndex(2);
		
		
		driver.findElementById("createLeadForm_sicCode").sendKeys("13345");
		
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("&*@$");
		
		
		driver.findElementById("createLeadForm_description").sendKeys("description");
		
		driver.findElementById("createLeadForm_importantNote").sendKeys("text note");
		
		//contact information
		
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("001");
		
		
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("91");
		
		WebElement phonenum = driver.findElementById("createLeadForm_primaryPhoneNumber");

		String mobile = "9978455211";
		phonenum.sendKeys(mobile);
		
		
		
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("abc@testleaf.com");
		
		
		
		
	//Primary Address
		
		driver.findElementById("createLeadForm_generalToName").sendKeys("Jordi");
		
		
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("100,1st Lane");
		
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("5th cross st");
		
		
		driver.findElementById("createLeadForm_generalCity").sendKeys("Miami");
		
		
		
		driver.findElementById("createLeadForm_generalStateProvinceGeoId").sendKeys("Florida");
		
		
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("33101");
		
		
		driver.findElementByXPath("//input[@name='submitButton']").click();
		
		Thread.sleep(2000);
		
		
		String vcompname =driver.findElementById("viewLead_companyName_sp").getText();
		
		System.out.println(vcompname);
		
		if (vcompname.contains(cname)) {
			
			System.out.println("matched");
			
		}	else {
				
				System.out.println("NotMatched");
				
				
			}
			

		driver.findElementByXPath("//a[text()='Edit']").click();
		
		driver.findElementByXPath("//input[@id='updateLeadForm_firstName']").clear();
		
		driver.findElementByXPath("//input[@id='updateLeadForm_firstName']").sendKeys("LeoMessi");
		
		driver.findElementByXPath("//input[@name='submitButton']").click();
		
		Thread.sleep(1000);		
		String editLead = driver.findElementByXPath("//span[@id='viewLead_firstName_sp']").getText();
		
		System.out.println(editLead);
	}

}
